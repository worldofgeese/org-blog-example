;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Rasmus

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.

;;; Code:

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-plus-contrib)
(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

;; setting to nil, avoids "Author: x" at the bottom
(setq user-full-name nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")


(setq org-publish-project-alist
      (list
       (list "posts"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
             :auto-sitemap t
             :sitemap-filename "index.org"
             :sitemap-title "Containers, Ubuntu & k8s"
             :sitemap-file-entry-format "%d *%t*"
             :html-head "<link rel=\"stylesheet\" href=\"https://unpkg.com/normalize.css\">
<link rel=\"stylesheet\" href=\"https://unpkg.com/concrete.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/site.css\">"
             :html-preamble "<div class=\"image-container\">
<img src=\"https://avatars2.githubusercontent.com/u/59834693?s=460&u=78b2bff39b59aa2f796e2f5299075448c1cc0ede&v=4\" alt=\"A goose head in front of the earth\"/>
</div>"
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "css"
          :base-directory "css/"
          :base-extension "css"
          :publishing-directory "./public/css"
          :publishing-function 'org-publish-attachment
          :recursive t)
       (list "all" :components '("posts" "css"))))

(provide 'publish)
;;; publish.el ends here
